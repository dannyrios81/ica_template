<?php
/**
 * Created by PhpStorm.
 * User: alix.rincon
 * Date: 23/03/2018
 * Time: 15:17
 */
namespace app\components;

use app\models\entities\Account;
use app\models\entities\Logs;
use app\models\entities\LogsParametricos;
use \app\models\entities\ValidityHasUsers;
use app\models\entities\Users;
use yii\base\Exception;
use yii\helpers\VarDumper;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class Utilities
{
    public static function arrayRoles($idUser = NULL)
    {

        if (!empty($idUser))
            $roles = \Yii::$app->authManager->getRolesByUser($idUser);
        else
            $roles = \Yii::$app->authManager->getRoles();

        $returnArray = [];
        foreach ($roles as $key => $rol) {
            if (!in_array($rol->name, array_keys(\Yii::$app->params['ProfileExclude'])))
                $returnArray[$rol->name] = $rol->name;
        }
        asort($returnArray, SORT_NATURAL | SORT_FLAG_CASE);

        return $returnArray;
    }
}
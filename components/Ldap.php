<?php
namespace app\components;



class Ldap
{
    private $client = null;

    private function getClientInt() {
        if($this->client == null)
        {
            // para que reconozca nuevas funciones del WS que vayas creando
            try
            {
                ini_set ('soap.wsdl_cache_enable',0);
                ini_set ('soap.wsdl_cache_ttl',0);
                ini_set('default_socket_timeout', 5);

                $this->client = new SoapClient(\Yii::$app->params['WSLdap'], array("connection_timeout" => 1));
            }
            catch (Exception $e)
            {
                throw $e;
            }
        }
        return $this->client;
    }
    public function getUserEmail($user)
    {
        try
        {
            return $this->getClientInt()->GetCorreo(array('UserName'=>$user));
        }
        catch(Exception $e)
        {
            return '';
        }
    }

    public static function AuthUsuarioDirect($user,$pass)
    {
        try
        {

//          ejemplo de autenticación
            $ldaprdn  = 'ICA\\'.$user;     // ldap rdn or dn
            $ldappass = $pass;  // associated password

//          conexión al servidor LDAP
          $ldapconn = ldap_connect(\Yii::$app->params['LdapServer']);
            if ($ldapconn) {

                // realizando la autenticación
                @$ldapbind = ldap_bind($ldapconn, $ldaprdn, $ldappass);

                // verificación del enlace
                if ($ldapbind) {
                    return true;
                } else {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch(Exception $e)
        {
            return false;
        }
    }
}

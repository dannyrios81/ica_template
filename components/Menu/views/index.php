<?php
/* @var $arrayMenu \app\components\Menu\Menu */

$this->registerJsFile('@web/js/Bootstrap-sidebar.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$arrayMenu->recorerArbol();
?>
<nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">

    <ul class="nav sidebar-nav">
        <?php echo $arrayMenu->dinamicHtmlMenu; ?>
    </ul>
</nav>

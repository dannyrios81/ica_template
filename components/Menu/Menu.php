<?php
namespace app\components\Menu;

use app\components\Utilities;
use app\models\entities\Appskeleton;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\VarDumper;


class Menu extends Widget
{
    /*
     * variables publicas que seran los parametros para
     * cargar informacion externa al widget
     */
    public $var1;
    public $var2;
    public $menuArray =array();
    public $dinamicHtmlMenu='';

    /**
     * metodo de inicializacion del objeto
     */
    public function init()
    {
        return parent::init();
    }
    /**
     * metodo de ejecucion del widget
     */
    public function run()
    {
        $this->nivel1();
        return $this->render('index',array('arrayMenu'=>$this));
    }
    protected function nivel1()
    {
        $this->menuArray[]=array('label'=>'Inicio','url' => array('/site/index'));
        if(\Yii::$app->user->isGuest)
        {
            $this->menuArray[]=array('label'=>'Iniciar Sesión','url' =>array('/site/login'));
        }
        else
        {
            $this->rolesNivel1();
            $this->menuArray[]=array('label' => 'Salir', 'url' => array('/site/logout'));
        }
    }
    protected function rolesNivel1()
    {
        foreach (Utilities::arrayRoles(\Yii::$app->user->id) as $value)
        {
            $id=uniqid();
            $this->menuArray[]=array('label'=>$value,'id'=>$id,'items'=>$this->itemsByRol($value,$id));
        }
    }
    protected function itemsByRol($profile,$idParentHTML,$level = 2,$idParent = null)
    {
        $menu = Appskeleton::find()
            ->innerJoin('app_skeleton_has_authitems','app_skeleton_has_authitems.idapp_skeleton = app_skeleton.idapp_skeleton')
            ->innerJoin('authitems','app_skeleton_has_authitems.auth_name=authitems.name AND authitems.name = :profile',array('profile'=>$profile))
            ->where(['idapp_skeleton_parent'=>$idParent,'levels'=>$level,'visible'=>1])
            ->all();
        $arrayReturn = array();

        foreach ($menu as $key => $value)
        {

            $id = uniqid();
            $temp =array();
            if(!empty($value->controllerid))
                $temp=array('label'=>$value->description,'idParentHTML'=>$idParentHTML,'id'=>$id,'url'=>array('/'.$value->controllerid.'/'.$value->actionid));
            else
                $temp=array('label'=>$value->description,'idParentHTML'=>$idParentHTML,'id'=>$id);

            $items = $this->itemsByRol($profile,$id,$value->levels+1,$value->idapp_skeleton);
            if(!empty($items))
            {
                $temp['items'] = $items;
            }

            $arrayReturn[]=$temp;

            unset($temp,$items);
        }
        return $arrayReturn;
    }

    public function recorerArbol($arMenu='',$idparent='',$level=0)
    {
        /*Recorrer Primer Nivel*/

        if(empty($arMenu))
            $arMenu=$this->menuArray;

        if (!$level)
            $this->dinamicHtmlMenu.='<li class="sidebar-brand"><a href="#">'.Html::encode(\Yii::$app->name).'</a></li>';
        else
            $this->dinamicHtmlMenu.='<ul id="'.$idparent.'" class="dropdown-menu" role="menu" style="margin-left:10px;background-color: #1a291f;">';

        foreach ($arMenu as $menu)
        {
            if(!isset($menu['items']))
            {
                $this->dinamicHtmlMenu.='<li>'.Html::a($menu['label'],$menu['url']).'</li>';
            }
            else
            {
                if(is_array($menu['items']) and count($menu['items'])>0)
                {
                    $this->dinamicHtmlMenu.='<li class="dropdown">'.Html::a($menu['label'].' '.Html::tag('span','',['class'=>'caret muted']),'#'.$menu['id'],['class'=>'dropdown-toggle','data-toggle'=>"collapse"]);
                    $this->recorerArbol($menu['items'],$menu['id'],$level+1);
                    $this->dinamicHtmlMenu.='</li>';
                }
            }
        }

        if($level>0)
            $this->dinamicHtmlMenu.='</ul>';

    }
}
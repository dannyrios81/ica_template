<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'ProfileExclude'=>['Publico'=>'?','Logueado'=>'@'],
    'descripcion'=>'Descripción breve del sistema de infromación',
    'disableLdap'=>true,
    'WSLdap'=>'http://www1.ica.gov.co/WebServices/LdapServices/UsersList.asmx?WSDL',
    'LdapServer'=>'ica.gov.co',//10.0.0.2
];

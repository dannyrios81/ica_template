<?php

return [
    'request' => [
        // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        'cookieValidationKey' => 'dfdsfsdfsdfsdfsdfsdfdssdfsf',
    ],
    'cache' => [
        'class' => 'yii\caching\FileCache',
    ],
    'user' => [
//        'identityClass' => 'app\models\entities\Users',
        'identityClass' => 'app\models\entities\Users',
        'enableAutoLogin' => false,
        'authTimeout' => 9000 //segundos de sesion
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
        'assignmentTable'=>'authassignment',
        'itemChildTable' => 'authitemchilds',
        'itemTable' => 'authitems',
        'ruleTable' => 'rules',
        // uncomment if you want to cache RBAC items hierarchy
//        'cache' => 'cache',
    ],
    'errorHandler' => [
        'errorAction' => 'site/error',
    ],
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'db' => $db,

    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
        ],
    ],
];
<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$components = require __DIR__ . '/components.php';

$config = [
    'id' => 'basic',
    'name'=>'Plantilla del ICA',
    'defaultRoute' => 'site/login',
    'homeUrl'=>['site/index'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'es-CO',
    'sourceLanguage' => 'es-CO',
    'timeZone' => 'America/Bogota',
    'components' => $components,
    'modules' => [
        'under-construction' => [
            'class' => '\mervick\underconstruction\Module',
            // this is the on off switch
            'locked' => false,
            // the list of IPs that are allowed to access site.
            // The default value is `['127.0.0.1', '::1']`, which means the site can only be accessed by localhost.
            'allowedIPs' => ['127.0.0.1'],
            // change this to your namespace, if you want use your own controller
            //'controllerNamespace' => 'mervick\underconstruction\controllers',
            // if you want use your views
            //'viewPath' => '@alias/to/viewPath',
            // default layout
            //'layout' => 'main',
            // if you want redirect to external website, default is null
            'redirectURL' => 'http://www.ica.gov.co',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '192.168.1.149', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '192.168.1.149', '::1'],
    ];
}

return $config;

<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "app_skeleton".
 *
 * @property int $idapp_skeleton
 * @property string $controllerid
 * @property string $actionid
 * @property string $description
 * @property int $idapp_skeleton_parent
 * @property int $levels
 * @property int $visible
 *
 * @property AppSkeleton $appSkeletonParent
 * @property AppSkeleton[] $appSkeletons
 * @property AppSkeletonHasAuthitems[] $appSkeletonHasAuthitems
 * @property Authitems[] $authNames
 */
class AppSkeleton extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'app_skeleton';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idapp_skeleton_parent', 'levels', 'visible'], 'integer'],
            [['controllerid', 'actionid', 'description'], 'string', 'max' => 100],
            [['idapp_skeleton_parent'], 'exist', 'skipOnError' => true, 'targetClass' => AppSkeleton::className(), 'targetAttribute' => ['idapp_skeleton_parent' => 'idapp_skeleton']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idapp_skeleton' => 'Idapp Skeleton',
            'controllerid' => 'Controllerid',
            'actionid' => 'Actionid',
            'description' => 'Description',
            'idapp_skeleton_parent' => 'Idapp Skeleton Parent',
            'levels' => 'Levels',
            'visible' => 'Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSkeletonParent()
    {
        return $this->hasOne(AppSkeleton::className(), ['idapp_skeleton' => 'idapp_skeleton_parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSkeletons()
    {
        return $this->hasMany(AppSkeleton::className(), ['idapp_skeleton_parent' => 'idapp_skeleton']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSkeletonHasAuthitems()
    {
        return $this->hasMany(AppSkeletonHasAuthitems::className(), ['idapp_skeleton' => 'idapp_skeleton']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthNames()
    {
        return $this->hasMany(Authitems::className(), ['name' => 'auth_name'])->viaTable('app_skeleton_has_authitems', ['idapp_skeleton' => 'idapp_skeleton']);
    }
}

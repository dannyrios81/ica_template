<?php

namespace app\models\entities;

use Yii;

/**
 * This is the model class for table "authitems".
 *
 * @property string $name
 * @property int $type
 * @property string $description
 * @property string $rule_name
 * @property resource $data
 * @property int $created_at
 * @property int $updated_at
 *
 * @property AppSkeletonHasAuthitems[] $appSkeletonHasAuthitems
 * @property AppSkeleton[] $appSkeletons
 * @property Authassignment[] $authassignments
 * @property Users[] $users
 * @property Authitemchilds[] $authitemchilds
 * @property Authitemchilds[] $authitemchilds0
 * @property Authitems[] $children
 * @property Authitems[] $parents
 * @property Rules $ruleName
 */
class Authitems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authitems';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => Rules::className(), 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'rule_name' => 'Rule Name',
            'data' => 'Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSkeletonHasAuthitems()
    {
        return $this->hasMany(AppSkeletonHasAuthitems::className(), ['auth_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSkeletons()
    {
        return $this->hasMany(AppSkeleton::className(), ['idapp_skeleton' => 'idapp_skeleton'])->viaTable('app_skeleton_has_authitems', ['auth_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthassignments()
    {
        return $this->hasMany(Authassignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['idusers' => 'user_id'])->viaTable('authassignment', ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthitemchilds()
    {
        return $this->hasMany(Authitemchilds::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthitemchilds0()
    {
        return $this->hasMany(Authitemchilds::className(), ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Authitems::className(), ['name' => 'child'])->viaTable('authitemchilds', ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParents()
    {
        return $this->hasMany(Authitems::className(), ['name' => 'parent'])->viaTable('authitemchilds', ['child' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(Rules::className(), ['name' => 'rule_name']);
    }
}

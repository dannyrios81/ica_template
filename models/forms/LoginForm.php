<?php

namespace app\models\forms;

use app\components\Ldap;
use app\models\entities\Users;
use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username','filter','filter' => 'strtolower'],
            // username and password are both required
            [['username', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        $authLdap = false;
        $disableLdap = \Yii::$app->params['disableLdap'];

        $user = $this->getUser();

        if($disableLdap)
        {
            if(empty($user))
                $this->addError('username','Usuario no encontrado');
        }
        else
        {
            if (!$this->hasErrors()) {

                if(!$user)
                {
                    $this->addError('username','El Usuario Ingresado No Se Encuentra Autorizado Para Usar Esta Aplicación');
                    return false;
                }

                if(!empty($user) && Ldap::AuthUsuarioDirect($this->username,$this->password))
                    $authLdap = true;
                else
                    $this->addError('username','El Usuario y/o Password Son Incorrectos.');

            }
        }


    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate())
        {
            return Yii::$app->user->login($this->getUser(), 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return Users|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->username);
        }
        return $this->_user;
    }
}

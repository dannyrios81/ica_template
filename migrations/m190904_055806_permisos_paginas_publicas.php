<?php

use app\models\entities\AppSkeleton;
use app\models\entities\AppSkeletonHasAuthitems;
use yii\db\Migration;
use yii\helpers\VarDumper;

/**
 * Class m190904_055806_permisos_paginas_publicas
 */
class m190904_055806_permisos_paginas_publicas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $publico = $auth->createRole('Publico');
        $publico->description='Publico';

        $logueado = $auth->createRole('Logueado');
        $logueado->description = 'Logueado';

        $administrador = $auth->createRole('Administrador');
        $administrador->description='Administrador';

        $auth->add($publico);
        $auth->add($logueado);
        $auth->add($administrador);

        $site = [
            ['controllerid'=>'site','actionid'=>'index','description'=>'Home','levels'=>2,'visible'=>0,'Perfiles'=>['Publico']],
            ['controllerid'=>'site','actionid'=>'login','description'=>'Login','levels'=>2,'visible'=>0,'Perfiles'=>['Publico']],
            ['controllerid'=>'site','actionid'=>'logout','description'=>'Logout','levels'=>2,'visible'=>1,'Perfiles'=>['Publico']],
            ['controllerid'=>'site','actionid'=>'error','description'=>'Error','levels'=>2,'visible'=>0,'Perfiles'=>['Publico']],
        ];

        foreach ($site as $item) {
            $appskeleton = new AppSkeleton();
            $appskeleton->load($item,'');
            $appskeleton->save();
            foreach ($item['Perfiles'] as $perfile) {
                $appskeletonhasauthitem = new AppSkeletonHasAuthitems(['idapp_skeleton'=>$appskeleton->idapp_skeleton,'auth_name' => $perfile]);
                $appskeletonhasauthitem->save();
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190904_055806_permisos_paginas_publicas cannot be reverted.\n";

        return false;
    }

}

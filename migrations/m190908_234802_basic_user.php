<?php

use yii\db\Migration;

/**
 * Class m190908_234802_basic_user
 */
class m190908_234802_basic_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $authManager = Yii::$app->getAuthManager();;

        $this->createTable('users',
            [
                'idusers'=>$this->primaryKey(11),
                'name'=>$this->string(100),
                'lastname'=>$this->string(100),
                'username'=>$this->string(100),
                'password'=>$this->string(100),
                'authkey'=>$this->string(),
                'token'=>$this->string()
            ],'DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci');

        $this->createIndex('UN_USERS_USERNAME','users','username',true);

        $this->addForeignKey('FK_AUTHASSIGMENT_USERS_IDUSERS',$authManager-> assignmentTable,'user_id','users','idusers');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190908_234802_basic_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190908_234802_basic_user cannot be reverted.\n";

        return false;
    }
    */
}

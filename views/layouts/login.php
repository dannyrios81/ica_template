<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\components\Menu\Menu;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::$app->name) ?></title>
    <?php $this->head() ?>
</head>
<body class="landing">
<?php $this->beginBody() ?>

<div id="wrapper">
    <div class="overlay"></div>

    <?= Menu::widget() ?>

    <div id="page-content-wrapper">
        <div class="container theme-showcase" id="page">
            <div class="container">
                <img src="<?php echo Yii::getAlias('@web/images/LogoIca_siscop.png') ?>" alt=""
                     class="logo-ica col-xs-4 col-sm-3 col-md-2">
                <img src="<?php echo Yii::getAlias('@web/images/logo_app.png') ?>" alt=""
                     class="logo-siscop col-xs-6 col-xs-offset-2 col-sm-5 col-sm-offset-4 col-md-3 col-md-offset-7">
            </div>
            <div id="header">
            </div><!-- header -->
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
            <br/>
            <div id="footer">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="copyright">
                            <?php echo Yii::$app->params['descripcion'] ?><br>
                            Copyright &copy; <?php echo date('Y'); ?> by Instituto Colombiano Agropecuario.<br>
                            Todos Los Derechos Reservados.<br>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="logo">
                            <img class="img-responsive" src="<?php echo Yii::getAlias('@web/images/Logo_Minagricultura_cuentas.png') ?>"
                                 alt="" style="width: 550px">
                        </div>
                    </div>
                </div>
            </div><!-- footer -->
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

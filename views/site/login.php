<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid main-bg-landing">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default center-block">
                <div class="panel-heading center-block">
                    <?= Html::encode($this->title) ?>
                </div>
                <div class="panel-body">
                    <?= Html::beginForm() ?>
                    <fieldset>
                        <?= Html::errorSummary($model,['class'=>'alert alert-block alert-danger']) ?>
                        <?= Html::activeTextInput($model,'username',['placeholder' => 'Usuario','autocomplete'=>'off','autofocus' => true,'class'=>'form-control'])?>
                        <?= Html::activePasswordInput($model,'password',['placeholder' => 'Contraseña','autocomplete'=>'off','class'=>'form-control']) ?>
                        <br><br>
                            <?= Html::submitButton('Ingresar', ['class' => 'btn btn-lg btn-success btn-block', 'name' => 'login-button', 'id'=>"login"]) ?>
                    </fieldset>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>
</div>